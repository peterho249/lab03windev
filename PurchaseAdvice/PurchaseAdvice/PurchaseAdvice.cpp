﻿// PurchaseAdvice.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "PurchaseAdvice.h"
#include <windowsX.h>
#include <WinUser.h>
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND japanPriceTextbox;
HWND singPriceTextbox;
HWND errorLabel1;
HWND errorLabel2;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct);
void OnCommand(HWND hwnd, int id, HWND hwndCtl, UINT codeNotify);
void OnPaint(HWND hwnd);
void OnDestroy(HWND hwnd);
char GetAdvice(HWND japanTextbox, HWND singTextbox);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PURCHASEADVICE, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PURCHASEADVICE));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON2));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_BTNFACE+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_PURCHASEADVICE);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON2));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 500, 300, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
		HANDLE_MSG(hWnd, WM_CREATE, OnCreate);
		HANDLE_MSG(hWnd, WM_COMMAND, OnCommand);
		HANDLE_MSG(hWnd, WM_PAINT, OnPaint);
		HANDLE_MSG(hWnd, WM_DESTROY, OnDestroy);
	case WM_CTLCOLORSTATIC:
	{
		if ((HWND)lParam == errorLabel1 || (HWND)lParam == errorLabel2)
		{
			SetBkMode((HDC)wParam, TRANSPARENT);
			SetTextColor((HDC)wParam, RGB(255, 0, 0));
			return (BOOL)GetSysColorBrush(COLOR_BTNFACE);
		}
		else
		{
			SetBkMode((HDC)wParam, TRANSPARENT);
			return (BOOL)GetSysColorBrush(COLOR_BTNFACE);
		}
	}
	break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

BOOL OnCreate(HWND hWnd, LPCREATESTRUCT lpCreateStruct)
{
	// Lấy font hệ thống
	LOGFONT lf;
	GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
	HFONT hFont = CreateFont(lf.lfHeight, lf.lfWidth,
		lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
		lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
		lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
		lf.lfPitchAndFamily, lf.lfFaceName);

	HWND hwnd = CreateWindowW(L"static", L"Nhập giá iPad tại Nhật Bản:",
		WS_CHILD | WS_VISIBLE | SS_LEFT, 50, 50, 200, 40, hWnd, nullptr, hInst, nullptr);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);
	hwnd = CreateWindowW(L"static", L"Nhập giá iPad tại Singapore:",
		WS_CHILD | WS_VISIBLE | SS_LEFT, 50, 100, 200, 40, hWnd, nullptr, hInst, nullptr);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);
	
	japanPriceTextbox = CreateWindowW(L"edit", L"", 
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER, 200, 50, 200, 20, hWnd, nullptr, hInst, nullptr);
	SendMessage(japanPriceTextbox, WM_SETFONT, WPARAM(hFont), TRUE);
	singPriceTextbox = CreateWindowW(L"edit", L"",
		WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER, 200, 100, 200, 20, hWnd, nullptr, hInst, nullptr);
	SendMessage(singPriceTextbox, WM_SETFONT, WPARAM(hFont), TRUE);
	
	hwnd = CreateWindowW(L"button", L"Tư vấn",
		WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 190, 150, 70, 30, hWnd, (HMENU)IDC_ADVICEBUTTON, hInst, nullptr);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);
	
	hwnd = CreateWindowW(L"static", L"JPY",
		WS_CHILD | WS_VISIBLE | SS_LEFT, 410, 50, 200, 40, hWnd, nullptr, hInst, nullptr);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);
	hwnd = CreateWindowW(L"static", L"SGD",
		WS_CHILD | WS_VISIBLE | SS_LEFT, 410, 100, 200, 40, hWnd, nullptr, hInst, nullptr);
	SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);
	
	errorLabel1= CreateWindowW(L"static", L"Bạn phải nhập giá hợp lệ!",
		WS_CHILD | SS_LEFT, 200, 75, 200, 20, hWnd, nullptr, hInst, nullptr);
	SendMessage(errorLabel1, WM_SETFONT, WPARAM(hFont), TRUE);
	errorLabel2 = CreateWindowW(L"static", L"Bạn phải nhập giá hợp lệ!",
		WS_CHILD | SS_LEFT, 200, 125, 200, 20, hWnd, nullptr, hInst, nullptr);
	SendMessage(errorLabel2, WM_SETFONT, WPARAM(hFont), TRUE);
	
	return TRUE;
}

void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id)
	{
	case IDC_ADVICEBUTTON:
	{
		char flag = GetAdvice(japanPriceTextbox, singPriceTextbox);
		if (flag == 1)
		{
			ShowWindow(errorLabel1, SW_HIDE);
			ShowWindow(errorLabel2, SW_HIDE);
			MessageBox(hWnd, L"Bạn nên mua iPad tại Nhật Bản!", L"Advice", 0);
		}

		if (flag == 2)
		{
			ShowWindow(errorLabel1, SW_HIDE);
			ShowWindow(errorLabel2, SW_HIDE);
			MessageBox(hWnd, L"Bạn nên mua iPad tại Singapore!", L"Advice", 0);
		}

		if (flag == 0)
		{
			ShowWindow(errorLabel1, SW_SHOW);
			ShowWindow(errorLabel2, SW_SHOW);
		}

		if (flag == -1)
		{
			ShowWindow(errorLabel1, SW_SHOW);
			ShowWindow(errorLabel2, SW_HIDE);
		}

		if (flag == -2)
		{
			ShowWindow(errorLabel1, SW_HIDE);
			ShowWindow(errorLabel2, SW_SHOW);
		}
	}
		break;
	case IDM_ABOUT:
		DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
		break;
	case IDM_EXIT:
		DestroyWindow(hWnd);
		break;
	}
}

void OnPaint(HWND hWnd)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	// TODO: Add any drawing code that uses hdc here...
	EndPaint(hWnd, &ps);
}

void OnDestroy(HWND hWnd)
{
	PostQuitMessage(0);
}

//Decide whether you should buy iPad in Singapore or Japan
//Param: handle of japanPriceTextbox and singPriceTextbox
//Return:
//			-2 - Missing price in Singapore
//			-1 - Missing price in Japan
//			0 - Missing all price
//			1 - You should choose Japan
//			2 - You should choose Singapore

char GetAdvice(HWND japanTextbox, HWND singTextbox)
{
	WCHAR* japanPriceBuffer;
	WCHAR* singPriceBuffer;
	int japanSize;
	int singSize;

	double japanPrice;
	double singPrice;

	double SGD_VNDRate = 16844.02f;
	double JPY_VNDRate = 203.48f;

	bool japanPriceValidFlag = TRUE;
	bool singPriceValidFlag = TRUE;

	japanSize = GetWindowTextLength(japanTextbox);
	singSize = GetWindowTextLength(singTextbox);

	//====================================================================
	//Check if text box blank
	if (japanSize == 0 || japanSize > 11)
		japanPriceValidFlag = FALSE;
	if (singSize == 0 || singSize > 11)
		singPriceValidFlag = FALSE;
	//====================================================================

	japanPriceBuffer = new WCHAR[japanSize + 1];
	singPriceBuffer = new WCHAR[singSize + 1];

	GetWindowText(japanTextbox, japanPriceBuffer, japanSize + 1);
	GetWindowText(singTextbox, singPriceBuffer, singSize + 1);

	//====================================================================
	//Check invalid number
	int i = 0;
	while (japanPriceValidFlag)
	{
		if (japanPriceBuffer[i] < L'0' || japanPriceBuffer[i] > L'9')
		{
			if (japanPriceBuffer[i] == 0)
				break;
			else
			{
				japanPriceValidFlag = FALSE;
				break;
			}
		}
		i++;
	}
	i = 0;
	while (singPriceValidFlag)
	{
		if (singPriceBuffer[i] < L'0' || singPriceBuffer[i] > L'9')
		{
			if (singPriceBuffer[i] == 0)
				break;
			else
			{
				singPriceValidFlag = FALSE;
				break;
			}
		}
		i++;
	}

	if (japanPriceValidFlag == FALSE && singPriceValidFlag == FALSE)
		return 0;
	if (japanPriceValidFlag == FALSE)
		return -1;
	if (singPriceValidFlag == FALSE)
		return -2;
	//====================================================================

	japanPrice = _wtof(japanPriceBuffer);
	singPrice = _wtof(singPriceBuffer);

	//====================================================================
	//Delete buffer
	if (!japanPriceBuffer)
		delete[] japanPriceBuffer;
	if (!singPriceBuffer)
		delete[] singPriceBuffer;
	//====================================================================

	//====================================================================
	//Covert price to VND
	japanPrice *= JPY_VNDRate;
	singPrice *= SGD_VNDRate;
	//====================================================================
	
	if (japanPrice > singPrice)
		return 2;
	else
		return 1;
}